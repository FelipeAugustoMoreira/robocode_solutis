package robocode;

import java.awt.*;

import java.awt.AWTException;

import java.awt.Robot;

import robocode.*;

public class Robo extends Robot {
	
	public static final double ACCELERATION = 1.0;
	public static final double DECELERATION = 2.0;
	public static final double GUN_TURN_RATE = 20.0;
	public static final double MAX_BULLET_POWER = 3.0;
	public static final double MAX_TURN_RATE = 10.0;
	public static final double MAX_VELOCITY = 8.0;
	public static final double MIN_BULLET_POWER = 0.1;
	public static final double RADAR_SCAN_RADIUS = 1200.0;
	public static final double RADAR_TURN_RATE = 45.0;
	public static final double ROBOT_HIT_BONUS = 1.2;
	public static final double ROBOT_HIT_DAMAGE = 0.6;
	
	
	protected static final long serialVersionUID = 1L;
	
	

	public Robo() throws AWTException {
		super();
		// TODO Auto-generated constructor stub
	}

	private void back(int i) {
		// TODO Auto-generated method stub
		
	}

	private void turnGunRight(int i) {
		// TODO Auto-generated method stub
		
	}

	private void ahead(int i) {
		// TODO Auto-generated method stub
		
	}
	
	public String getName() {
		ahead(100); 
		return "King";
	}
	
	public void Run() {
		do {
			ahead(100);
			turnGunRight(360);
			back(100);
			turnGunRight(360);
		} 
		while(true);
	}
	
	public void fire (double intensidade){
		
		int dano = (int) (4*intensidade);
	}
	
}
